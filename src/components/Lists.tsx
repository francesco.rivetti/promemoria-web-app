import React, {Component} from "react";
import {Color} from "../models/Color.enum";
import {Reminder} from "../models/Reminder";
import {List} from "../models/List";
import '../App.css';

interface ListsProps {

}

export default class Lists extends Component <ListsProps> {
    constructor(props: ListsProps) {
        super(props)
    }

    render() {
        const elenchi: List[] = [
            new List("Case", 0, Color.Purple),
            new List("Libri", 1, Color.Lightblu, [new Reminder({name: "Test", order: 0})]),
            new List("Auto", 2, Color.Green, [new Reminder({name: "Test", order: 0}),new Reminder({name: "Test", order: 0})]),
            new List("Viaggi", 3, Color.Yellow, [new Reminder({name: "Test", order: 0})]),
            new List("Fogli di giornale", 3, Color.Orange, ),
        ]
        return<div>
            <h1>Lists</h1>
            {elenchi.map((elenco, index) => {
                return <p><span className="dot" style={{backgroundColor: elenco.color}}></span> {elenco.name} {elenco.reminders.length}</p>
            })}
        </div>
    }
}
